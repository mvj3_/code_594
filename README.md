 实现各种声音效果，包括调整声音的大小（Volume）、音调（Pitch）、声像（Pan）、静音（Mute），还可以调整声音的淡入淡出效果，背景音乐混合（Audio track）效果。代码例子中还提供了一个飞机射击游戏，当射中目标之后，会发出声音。	
